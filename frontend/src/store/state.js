let state = {
    Doctor: {
        name: "",
        specialty: "",
        healthPlan: ""
    },

    Patient: {
        name: "",
        healthPlan: ""
    },

    Consult: {
        idConsult: "",
        idPat: "",
        idDoc: "",
        dateConsult: null,
        hourConsult: null,
        namePat: "",
        nameDoc: ""
    },
    Patients: [],

    Doctors: [],

    Consults: []
}

export default state;