import Vue from "vue";
import Vuex from "vuex";
import state from "./state"
import AxiosService from "../utils/axios-service.js";

Vue.use(Vuex);

export const store = new Vuex.Store({
  strict: true,
  state,
  mutations: {
    updatePatient(state, pat){
        state.patient = pat
    },

    updatePatientName(state, name){
        state.Patient.name = name
    },

    updatePatientHP(state, hp){
        state.Patient.healthPlan = hp
    },

    updateDoctor(state, doc){
        state.Doctor = doc
    },

    updateDoctorName(state, name){
        state.Doctor.name = name
    },

    updateDoctorSpecialty(state, spec){
        state.Doctor.specialty = spec
    },

    updateDoctorHP(state, hp){
        state.Doctor.healthPlan = hp
     },

    updatePatients(state, patients){
      state.Patients = patients;
    },

    updateDoctors(state, doctors){
        state.Doctors = doctors;
    },

    updateConsults(state, consults){
      state.Consults = consults;
    },

    updateConsultIdPat(state, idPat){
      state.Consult.idPat = idPat;
    },

    updateConsultIdDoc(state, idDoc){
      state.Consult.idDoc = idDoc;
    },
    
    updateConsultDate(state, dateConsult){
      state.Consult.dateConsult = dateConsult;
    },

    updateConsultHour(state, hourConsult){
      state.Consult.hourConsult = hourConsult;
    },

    updateConsultNamePat(state, namePat){
      state.Consult.namePat = namePat;
    },

    updateConsultNameDoc(state, nameDoc){
      state.Consult.nameDoc = nameDoc;
    },

  },
  actions: {
    getDoctor({commit}, id){
      AxiosService.getMedico(id)
      .then(response => {
        commit("updateDoctor", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    addDoctor(){
      AxiosService.addDoctor(state.Doctor)
      .then(response => {
        // eslint-disable-next-line
        console.log(response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    createRequest(){
      // eslint-disable-next-line
      console.log(this.state.Consult);
      AxiosService.createRequest(this.state.Consult)
      .then(response => {
        console.log(response.data);
      })
      .catch(error => {
        console.log(error.response.data);
      })
    },

    getDoctorsbyName({commit}){
      AxiosService.getDoctorsbyName(this.state.Doctor.name)
      .then(response => {
        commit("updateDoctors", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    getDoctorsbySpec({commit}){
      AxiosService.getDoctorsbySpec(this.state.Doctor.specialty)
      .then(response => {
        commit("updateDoctors", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    getDoctorsbyHP({commit}){
      AxiosService.getDoctorsbyHP(this.state.Doctor.healthPlan)
      .then(response => {
        commit("updateDoctors", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    getDoctors({commit}){
      AxiosService.getDoctors()
      .then(response => {
        commit("updateDoctors", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    addPatient(){
      AxiosService.addPatient(state.Patient)
      .then(response => {
        // eslint-disable-next-line
        console.log(response.data);
      })
      .catch(error => {
        console.log(error.response.data);
      })
    },

    getPatient({commit}, id){
      AxiosService.getPatient(id)
      .then(response => {
        commit("updatePatients", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    getPatientsbyName({commit}){
      AxiosService.getPatientsbyName(this.state.Patient.name)
      .then(response => {
        commit("updatePatients", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    getPatientsbyHP({commit}){
      AxiosService.getPatientsbyHP(this.state.Patient.healthPlan)
      .then(response => {
        commit("updatePatients", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    getPatients({commit}){
      AxiosService.getPatients()
      .then(response => {
        commit("updatePatients", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },

    getConsults({commit}){
      AxiosService.getConsults()
      .then(response => {
        commit("updateConsults", response.data);
      })
      .catch(error => {
        // eslint-disable-next-line
        console.log(error.response.data);
      })
    },
  },
  modules: {}
});
