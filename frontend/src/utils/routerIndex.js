import Vue from 'vue';
import router from 'vue-router';
import CadastroMedico from '../components/CadastroMedico/cadMed.vue';
import CadastroPessoa from '../components/PatientRegister/patReg.vue';
import MenuPage from '../components/MenuPage/menuPage.vue';
import SearchConsult from '../components/SearchMedicalConsultation/searchConsult.vue';
import PatientSearch from '../components/PatientSearch/patSearch.vue';
import DoctorSearch from '../components/DoctorSearch/docSearch.vue';
import MakeAppointment from '../components/Appointment/makeAppointment.vue';

Vue.use(router);
export default new router({
    routes: [
        {
            path: '/',
            name: 'MenuPage',
            component: MenuPage 
        },
        {
            path: '/CadastroMedico',
            name: 'CadastroMedico',
            component: CadastroMedico 
        },
        {
            path: '/PatientRegister',
            name: 'PatientRegister',
            component: CadastroPessoa
        },
        {
            path: '/SearchConsult',
            name: 'SearchConsult',
            component: SearchConsult
        },
        {
            path: '/PatientSearch',
            name: 'PatientSearch',
            component: PatientSearch
        },
        {
            path: '/DoctorSearch',
            name: 'DoctorSearch',
            component: DoctorSearch
        },
        {
            path: '/MakeAppointment',
            name: 'MakeAppointment',
            component: MakeAppointment 
        },
    ]})