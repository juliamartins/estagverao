import Axios from 'axios'

const RESOURCE_NAME = '/api'

export default{

    // DOCTOR //
    getDoctorsbyName(name){
        return Axios.get(`${RESOURCE_NAME}/GlobalContract/getDoctorsbyName/${name}`)
        //cria http://localhost:5000/medicos
    },
    getDoctorsbySpec(specialty){
        return Axios.get(`${RESOURCE_NAME}/GlobalContract/getDoctorsbySpec/${specialty}`)
        //cria http://localhost:5000/medicos
    },
    getDoctorsbyHP(hp){
        return Axios.get(`${RESOURCE_NAME}/GlobalContract/getDoctorsbyHP/${hp}`)
        //cria http://localhost:5000/medicos
    },

    getDoctors(){
        return Axios.get(`${RESOURCE_NAME}/GlobalContract/getDoctors`)
    },

    // PATIENT //
    getPatientsbyName(name){
        return Axios.get(`${RESOURCE_NAME}/GlobalContract/getPatientsbyName/${name}`)
    },
    getPatientsbyHP(hp){
        return Axios.get(`${RESOURCE_NAME}/GlobalContract/getPatientsbyHP/${hp}`)
    },

    getPatients(){
        return Axios.get(`${RESOURCE_NAME}/GlobalContract/getPatients/`)
    },

    // CONSULT //
    getConsults(){
        return Axios.get(`${RESOURCE_NAME}/GlobalContract/getConsults`)
    },

    //post
    addPatient(patient){
        return Axios.post(`${RESOURCE_NAME}/GlobalContract/addPatient`, patient)
    },

    addDoctor(doctor){
        return Axios.post(`${RESOURCE_NAME}/GlobalContract/addDoctor`, doctor)
    },

    doctorAccept(consult){
        return Axios.post(`${RESOURCE_NAME}/GlobalContract/doctorAccept`, consult)
    },

    doctorRejects(consult){
        return Axios.post(`${RESOURCE_NAME}/GlobalContract/doctorRejects`, consult)
    },

    createRequest(consult){
        return Axios.post(`${RESOURCE_NAME}/GlobalContract/createRequest`, consult)
    }

}