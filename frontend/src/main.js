import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import {store} from './store'
import { BootstrapVue} from 'bootstrap-vue'
import router from './utils/routerIndex.js'
import Axios from 'axios'

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(Vuex)
Vue.use(router)

const baseURL = "http://127.0.0.1:5000/"

Axios.defaults.baseURL = baseURL

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')