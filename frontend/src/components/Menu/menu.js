import CadMed from '../CadastroMedico/cadMed.vue'
import RegPat from '../PatientRegister/patReg.vue'
import SearchConsult from '../SearchMedicalConsultation/searchConsult.vue'
import MakeAppointment from '../Appointment/makeAppointment'

export default {
  name: 'Menu',
  components: {
    CadMed,
    RegPat,
    SearchConsult,
    MakeAppointment
  },
  data() {
    return {
        links: [
            {
                id: 1,
                name: 'Doctor Register',
                path: '/CadastroMedico',
            },
            {
                id: 2,
                name: 'Patient Register',
                path: '/PatientRegister',
            },
            {
                id: 3,
                name: 'Doctor Search',
                path: '/DoctorSearch',
            },
            {
                id: 4,
                name: 'Patient Search',
                path: '/PatientSearch',
            },
            {
                id: 5,
                name: 'Make an Appointment',
                path:'/MakeAppointment',
            },
            {
                id: 6,
                name: 'Search Medical Consultation',
                path:'/SearchConsult',
            },
            
        ],
    }
 }
}