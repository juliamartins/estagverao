export default {
    name: 'state',
    components: {
    },
    computed: {
        doctor: {
            get(){
                return this.$store.state.Doctor
            },
            set(value){
                this.$store.commit("updateDoctor", value)
            }
        },
        doctorName: {
            get(){
                return this.$store.state.Doctor.name
            },
            set(value){
                this.$store.commit("updateDoctorName", value)
            }
        },
        doctorSpecialty: {
            get(){
                return this.$store.state.Doctor.specialty
            },
            set(value){
                this.$store.commit("updateDoctorSpecialty", value)
            }
        },
        doctorHealthPlan: {
            get(){
                return this.$store.state.Doctor.healthPlan
            },
            set(value){
                this.$store.commit("updateDoctorHP", value)  
            }
        }
    },
    methods: {
        addDoctor() {
            this.$store.dispatch("addDoctor");
        }
    }
 }