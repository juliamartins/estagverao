export default {
    name: 'state',
    components: {
    },
    computed: {
        doctors: {
            get(){
                return this.$store.state.Doctors
            }
        },
        doctorName: {
            get(){
                return this.$store.state.Doctor.name
            },
            set(value){
                this.$store.commit("updateDoctorName", value)
            }
        },
        doctorSpec: {
            get(){
                return this.$store.state.Doctor.healthPlan
            },
            set(value){
                this.$store.commit("updateDoctorSpecialty", value)  
            }
        },
        doctorHealthPlan: {
            get(){
                return this.$store.state.Doctor.healthPlan
            },
            set(value){
                this.$store.commit("updateDoctorHP", value)  
            }
        },
    },
    methods: {
        getDoctorsbyName() {
            this.$store.dispatch("getDoctorsbyName");
        },
        getDoctorsbySpec() {
            this.$store.dispatch("getDoctorsbySpec");
        },
        getDoctorsbyHP() {
            this.$store.dispatch("getDoctorsbyHP");
        }
    }
 }