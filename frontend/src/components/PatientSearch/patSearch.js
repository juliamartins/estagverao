export default {
    name: 'state',
    components: {
    },
    computed: {
        patients: {
            get(){
                return this.$store.state.Patients
            }
        },
        patientName: {
            get(){
                return this.$store.state.Patient.name
            },
            set(value){
                this.$store.commit("updatePatientName", value)
            }
        },
        patientHealthPlan: {
            get(){
                return this.$store.state.Patient.healthPlan
            },
            set(value){
                this.$store.commit("updatePatientHP", value)  
            }
        }
    },
    methods: {
        getPatientsbyName() {
            this.$store.dispatch("getPatientsbyName");
        },
        getPatientsbyHP() {
            this.$store.dispatch("getPatientsbyHP");
        }
    }
 }