import { mapState } from "vuex"

export default {
    name: 'state',
    components: {
    },
    computed: {
        // ...mapState({
        //     patient: state => state.patient
        // }),
        patient: {
            get(){
                return this.$store.state.Patient
            },
            set(value){
                this.$store.commit("updatePatient", value)
            }
        },
        patientName: {
            get(){
                return this.$store.state.Patient.name
            },
            set(value){
                this.$store.commit("updatePatientName", value)
            }
        },
        patientHealthPlan: {
            get(){
                return this.$store.state.Patient.healthPlan
            },
            set(value){
                this.$store.commit("updatePatientHP", value)  
            }
        }
    },
    methods: {
        addPatient() {
            this.$store.dispatch("addPatient");
        }
    }
 }