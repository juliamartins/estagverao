export default {
    name: 'state',
    components: {
    },
    computed: {
        consults: {
            get(){
                return this.$store.state.Consults
        }
    },
},

    beforeCreate: function() {
                this.$store.dispatch("getConsults");
    },
            
    methods: {
        getConsults() {
            this.$store.dispatch("getConsults");
        }
    }
}
