// new Vue({
//     beforeCreate: function() {
//         this.$store.dispatch("getPatients");
//         this.$store.dispatch("getDoctors");
//     }
// })

export default {
    name: 'state',
    components: {
    },
    computed: {
        doctors: {
            get(){
                return this.$store.state.Doctors
            }
        },
        patients: {
            get(){
                return this.$store.state.Patients
            }
        },

        consult: {
            get(){
                return this.$store.state.Consult
            },
            set(value){
                this.$store.commit("updateConsult", value)
            }
        },
        consultDate: {
            get(){
                return this.$store.state.Consult.dateConsult
            },
            set(value){
                this.$store.commit("updateConsultDate", value)
            }
        },
        consultHour: {
            get(){
                return this.$store.state.Consult.hourConsult
            },
            set(value){
                this.$store.commit("updateConsultHour", value)  
            }
        },
        consultNamePat: {
            get(){
                return this.$store.state.Consult.namePat
            },
            set(value){
                this.$store.commit("updateConsultNamePat", value)
            }
        },
        consultNameDoc: {
            get(){
                return this.$store.state.Consult.nameDoc
            },
            set(value){
                this.$store.commit("updateConsultNameDoc", value)  
            }
        },
        consultIdPat: {
            get(){
                return this.$store.state.Consult.idPat
            },
            set(value){
                this.$store.commit("updateConsultIdPat", parseInt(value))
            }
        },
        consultIdDoc: {
            get(){
                return this.$store.state.Consult.idPat
            },
            set(value){
                this.$store.commit("updateConsultIdDoc", parseInt(value))
            }
        },
    },

    beforeCreate: function() {
                this.$store.dispatch("getPatients");
                this.$store.dispatch("getDoctors");
            },
            
    methods: {
        getPatient(idPat, namePat){
            this.$store.commit("updateConsultIdPat", parseInt(idPat))
            this.$store.commit("updateConsultNamePat", namePat)
        },

        getDoctor(idDoc, nameDoc){
            this.$store.commit("updateConsultIdDoc", parseInt(idDoc))
            this.$store.commit("updateConsultNameDoc", nameDoc) 
        },
        createRequest(consult){
            this.$store.dispatch("createRequest", consult);
        }
        // getPatients() {
        //     this.$store.dispatch("getPatients");
        // },
        // getDoctors() {
        //     this.$store.dispatch("getDoctors");
        // },
    },
   
}