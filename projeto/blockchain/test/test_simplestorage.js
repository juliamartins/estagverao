const rlp = require("rlp");
// const rlpExample = artifacts.require("RlpExample")
// const assert = require("assert");

const GlobalContract = artifacts.require("GlobalContract");
const PatientContract = artifacts.require("PatientContract");
const DoctorContract = artifacts.require("DoctorContract");
const ConsultationContract = artifacts.require("ConsultationContract");
contract('GlobalContract', async(accounts) => {
    let globalInst;
    let patientInst;
    let doctorInst;
    let consultInst;

    beforeEach( async() =>{
        patientInst = await PatientContract.new();
        doctorInst = await DoctorContract.new();
        consultInst = await ConsultationContract.new(patientInst.address, doctorInst.address);
        globalInst = await GlobalContract.new(patientInst.address, doctorInst.address, consultInst.address);
    })

    //TESTE MARCAÇÂO DE CONSULTAS
    
    // it('Adicionar Consulta', async() => {
    //     let encoded = rlp.encode([111, "sofia", "public"]);
    //     const addPat = await globalInst.addPatient(encoded);

        

    //     let encoded3 = rlp.encode([123, 111, 222, 1003, 20, "julia", "gabriel"]);
    //     console.log(encoded3.toString('hex'));
    //     const result = await globalInst.createRequest(encoded3);

    //     console.log("retorno: " + JSON.stringify(result));

    //     //const consults = await globalInst.doctorReceives();
    //     //console.log("consult: " + consults);
    // }).timeout(8000)

    // it('Pegar consulta específica', async() => {
    //     let encodedP = rlp.encode([111, "sofia", "public"]);
    //     const addPat = await globalInst.addPatient(encodedP);

    //     let encodedD = rlp.encode([444, "Guilherme", "Cardiologista", "public"]);
    //     const addDoc = await globalInst.addDoctor(encodedD);

    //     let encoded = rlp.encode([123, 111, 444, 1003, 20, "julia", "gabriel"]);
    //     let encoded2 = rlp.encode([124, 111, 444, 1004, 20, "julia", "gabriel"]);

    //     console.log("1 hexa enviado: ", encoded.toString('hex'));
    //     console.log("2 hexa enviado: ", encoded2.toString('hex'));

    //     const setconsult = await consultInst.createRequest(encoded);
    //     const setconsult2 = await consultInst.createRequest(encoded2);

    //     const getconsult = await consultInst.getConsult(123);
    //     const getconsult2 = await consultInst.getConsult(124);

    //     console.log("1 hexa recebido: ", getconsult.toString('hex'));
    //     console.log("2 hexa recebido: ", getconsult2.toString('hex'));

    //     //const numberConsult = await consultInst.numberConsults();
    //     //console.log("Numero de consultas pendentes: ", JSON.stringify(numberConsult));
    // }).timeout(10000)

    it('Pegar consultas', async() => {
        let encodedP = rlp.encode([111, "sofia", "public"]);
        const addPat = await globalInst.addPatient(encodedP);

        let encodedD = rlp.encode([444, "Guilherme", "Cardiologista", "public"]);
        const addDoc = await globalInst.addDoctor(encodedD);

        let encoded = rlp.encode([123, 111, 444, 1003, 20, "sofia", "Guilherme"]);
        let encoded2 = rlp.encode([125, 111, 444, 1004, 20, "sofia", "Guilherme"]);
        let encoded3 = rlp.encode([128, 111, 444, 1004, 21, "sofia", "Guilherme"]);

        console.log("1 hexa enviado: ", encoded.toString('hex'));
        console.log("2 hexa enviado: ", encoded2.toString('hex'));
        console.log("2 hexa enviado: ", encoded3.toString('hex'));

        const setconsult = await globalInst.createRequest(encoded);

        const idConsult = await globalInst.returnIDConsult();
        console.log("Id Consult: ", idConsult.toString());
        const setconsult2 = await globalInst.createRequest(encoded2);
        const setconsult3 = await globalInst.createRequest(encoded3);

        const getconsults = await globalInst.getConsults();
        console.log("hexa de consultas recebido: ", getconsults.toString('hex'));

    }).timeout(12000)

    // it('Doctor Accept', async() => {
    //     let encodedP = rlp.encode([111, "sofia", "public"]);
    //     const addPat = await globalInst.addPatient(encodedP);

    //     let encodedD = rlp.encode([444, "Guilherme", "Cardiologista", "public"]);
    //     const addDoc = await globalInst.addDoctor(encodedD);

    //     let encoded = rlp.encode([123, 111, 444, 1003, 20, "sofia", "Guilherme"]);
    //     let encoded2 = rlp.encode([124, 111, 444, 1004, 20, "sofia", "Guilherme"]);

    //     console.log("1 hexa enviado: ", encoded.toString('hex'));
    //     console.log("2 hexa enviado: ", encoded2.toString('hex'));

    //     const setconsult = await globalInst.createRequest(encoded);
    //     const setconsult2 = await globalInst.createRequest(encoded2);

    //     const getconsults = await globalInst.getConsults();
    //     console.log("hexa de consultas recebido: ", getconsults.toString('hex'));

    //     // // DOCTOR ACCEPT
    //     // const doctorAccept = await globalInst.doctorAccept(encoded);

    //     // // DOCTOR REJECTS
    //     const doctorRejects = await globalInst.doctorRejects(encoded);

    //     const getconsults2 = await globalInst.getConsults();
    //     console.log("hexa de consultas recebido após o reject: ", getconsults2.toString('hex'));

    //     // // PEGAR TODAS AS CONSULTAS ACEITAS
    //     const getConsultsAccept = await globalInst.getConsultsAccept();
    //     console.log("hexa de consultas aceitas: ", getConsultsAccept.toString('hex'));


    //     // // PEGAR UMA CONSULTA ACEITA
    //     //const getConsultAccept = await globalInst.getConsultAccept(123);
    //     //console.log("hexa de consultas aceitas: ", getConsultAccept.toString('hex'));

    //     // // TESTE DOCTOR RECEIVES
    //     // const doctorReceives = await globalInst.doctorReceives();
    //     // console.log("hexa de consultas recebido com o DoctorReceives: ", doctorReceives.toString('hex'));

    // }).timeout(20000)


    ////////////////////////////////////////////////////////////// Teste Paciente //////////////////////////////////////////////////////////////

    // it('Adicionar Paciente', async() => {     
    //     let encoded = rlp.encode([1, "sofia", "public"]);
    //     const addPat = await globalInst.addPatient(encoded);

    //     let encoded2 = rlp.encode([2, "sofia", "public"]);
    //     const addPat2 = await globalInst.addPatient(encoded2);

    //     const returnID = await globalInst.returnID();
    //     console.log(returnID.toString());
    // });

    // it('Numero de Pacientes', async() => {   
    //     let encoded = rlp.encode([222, "sofia", "public"]);
    //     const addPat = await globalInst.addPatient(encoded);

    //     let encoded2 = rlp.encode([333, "alice", "public"]);
    //     const addPat2 = await globalInst.addPatient(encoded2);
    //     const numberPat = await globalInst.numberOfPatients();
    //     console.log("Número de pacientes: " + JSON.stringify(numberPat));
    //      assert.equal(numberPat, 2);
    // });
    
    // it('Ver Paciente', async() => {   
    //     let encoded = rlp.encode([222, "sofia", "public"]);
    //     console.log(encoded.toString('hex'));
    //     const addPat = await globalInst.addPatient(encoded);
    //     const patient = await globalInst.getPatient(222);
    //     console.log("Paciente: ", patient)
    // });

    // it('Ver Pacientes', async() => {
    //     let encoded = rlp.encode([222, "sofia", "public"]);
    //     const addPat = await globalInst.addPatient(encoded);

    //     let encoded2 = rlp.encode([111, "julia", "public"]);
    //     const addPat2 = await globalInst.addPatient(encoded2);

    //     let encoded3 = rlp.encode([333, "alice", "public"]);
    //     const addPat3 = await globalInst.addPatient(encoded3);

    //     const patients = await globalInst.getPatients();
    //     console.log("Pacientes: ", patients)
    // });

    // it('Ver Pacientes por nome', async() => {  
    //     let encoded = rlp.encode([222, "sofia", "public"]);
    //     const addPat = await globalInst.addPatient(encoded);
    //     console.log("sofia: ", encoded.toString('hex'));

    //     let encoded2 = rlp.encode([111, "julia", "public"]);
    //     const addPat2 = await globalInst.addPatient(encoded2);
    //     console.log("julia: ", encoded2.toString('hex'));

    //     let encoded3 = rlp.encode([333, "alice", "private"]);
    //     const addPat3 = await globalInst.addPatient(encoded3);
    //     console.log("alice: ", encoded3.toString('hex'));

    //     let encoded4 = rlp.encode([444, "julia", "private"]);
    //     const addPat4 = await globalInst.addPatient(encoded4);
    //     console.log("julia: ", encoded4.toString('hex'));

    //     let encoded5 = rlp.encode([555, "julia", "public"]);
    //     const addPat5 = await globalInst.addPatient(encoded5);
    //     console.log("julia: ", encoded5.toString('hex'));

    //     //RETORNA O PACIENTE PELO NOME
    //     // const getPatsbyName = await globalInst.getPatientsbyName("julia");
    //     // console.log("Todos os Pacientes chamados julia: ", getPatsbyName);

    //     //RETORNA O PACIENTE PELO PLANO DE SAUDE
    //     const getPatientsbyHP = await globalInst.getPatientsbyHP("private");
    //     console.log("Todos os Pacientes com plano privado: ", getPatientsbyHP);
    // });

    // it('Ver Pacientes por plano de saúde', async() => {  
    //     let encoded = rlp.encode([222, "sofia", "public"]);
    //     console.log(encoded.toString('hex'));
    //     const addPat = await globalInst.addPatient(encoded);

    //     let encoded2 = rlp.encode([333, "alice", "private"]);
    //     console.log(encoded2.toString('hex'));
    //     const addPat2 = await globalInst.addPatient(encoded2);

    //     const getPatsbyHP = await globalInst.getPatientsbyHP("public");
    //     console.log("Todos os Pacientes com o plano de saúde público: ", getPatsbyHP);
    // });

    ////////////////////////////////////////////////////////////// Teste Medico //////////////////////////////////////////////////////////////

    // it('Adicionar Médico', async() => {
    //     let encoded = rlp.encode([444, "Guilherme", "Cardiologista", "public"]);
    //     const addPat = await globalInst.addDoctor(encoded);
    // });

    // it('Numero de Médicos', async() => {   
    //     let encoded = rlp.encode([444, "Guilherme", "Cardiologista", "public"]);
    //     const addPat = await globalInst.addDoctor(encoded);

    //     let encoded2 = rlp.encode([222, "Sofia", "Ortopedista", "public"]);
    //     const addPat2 = await globalInst.addDoctor(encoded2);

    //     const numberDoc = await globalInst.numberOfDoctors();
    //     console.log("Número de médicos: " + JSON.stringify(numberDoc));
    // });

    // it('Ver Médico', async() => {   
    //     let encoded = rlp.encode([444, "Guilherme", "Cardiologista", "public"]);
    //     console.log(encoded.toString('hex'));
    //     const addPat = await globalInst.addDoctor(encoded);

    //     const getDoc = await globalInst.getDoctor(444);
    //     console.log("Médico: ", getDoc);
    // });

    // it('Ver Médicos', async() => {
    //     let encoded = rlp.encode([444, "Guilherme", "Cardiologista", "public"]);
    //     const addPat = await globalInst.addDoctor(encoded);

    //     let encoded2 = rlp.encode([222, "Sofia", "Ortopedista", "public"]);
    //     const addPat2 = await globalInst.addDoctor(encoded2);

    //     const getDoctors = await globalInst.getDoctors();
    //     console.log("Todos os médicos: ", getDoctors);
    // });

    // it('Ver Médicos por nome', async() => {  
    //     let encoded = rlp.encode([444, "Guilherme", "Cardiologista", "public"]);
    //     const addPat = await globalInst.addDoctor(encoded);

    //     let encoded2 = rlp.encode([222, "Sofia", "Ortopedista", "public"]);
    //     const addPat2 = await globalInst.addDoctor(encoded2);
    //     console.log(encoded2.toString('hex'));

    //     const getDoctors = await globalInst.getDoctorsbyName("Guilherme");
    //     console.log("Todos os médicos chamados sofia: ", getDoctors);
    // });

});