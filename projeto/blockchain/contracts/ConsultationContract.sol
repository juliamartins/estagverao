pragma solidity >=0.4.0 <0.7.0;
pragma experimental ABIEncoderV2;
import './PatientContract.sol';
import './DoctorContract.sol';
import '../node_modules/solidity-rlp/contracts/RLPReader.sol';



contract ConsultationContract {
    uint storedData;
    PatientContract patientContract;
    DoctorContract doctorContract;

    using RLPReader for RLPReader.RLPItem;
    using RLPReader for bytes;

    uint idConsultation;

    constructor(address PatientAdress, address DoctorAdress) public{
        patientContract = PatientContract(PatientAdress);
        doctorContract = DoctorContract(DoctorAdress);
        idConsultation = 0;
    }

    uint[] arrayConsult;
    uint[] arrayConsultAccept;


    mapping(uint => bytes) mapConsult;
    mapping(uint => bytes) mapConsultAccept;

    // enum Stages {
    //     RequestingConsult,
    //     WaitingAccept,
    //     Finish
    // }

    // Stages public stage = Stages.RequestingConsult;

    //  modifier atStage(Stages _stage) {
    //     require(stage == _stage,
    //         "Function cannot be called at this time."
    //     );
    //     _;
    // }

    // modifier transitionAfter() {
    //     _;
    //     nextStage();
    // }

    //  modifier timedTransitions() {
    //     if (stage == Stages.RequestingConsult) {
    //         nextStage();
    //     }
    //     if (stage == Stages.WaitingAccept) {
    //         nextStage();
    //     }
    //     _;
    // }

    function returnIDConsult() public view returns(uint){
        return idConsultation;
    }

    // event teste(string pat, string doc);

    function createRequest(bytes memory consult)
    public{
        RLPReader.RLPItem[] memory ls = consult.toRlpItem().toList();

        uint i;

        uint idConsult = ls[0].toUint();
        uint idPat = ls[1].toUint();
        uint idDoc = ls[2].toUint();
        uint dateConsult = ls[3].toUint();
        uint hourConsult = ls[4].toUint();

        bool ok = false;

        // if(dateConsult == 0 || hourConsult == 0){
        //         return();
        // }else{
            // if((patientContract.patientExists(idPat) == false) && (doctorContract.doctorExists(idDoc) == false)){
            //     return();
            // } else {
                if(arrayConsult.length == 0){
                    idConsultation = idConsult;
                    mapConsult[idConsultation] = consult;
                    arrayConsult.push(idConsultation);
                } else {
                    for(i = 0; i < arrayConsult.length; i ++){
                        if(idConsultExists(idConsult)){
                            return();
                        } else {
                            if(dateExists(dateConsult)){
                                if(hourExists(hourConsult)){
                                    return();
                                } else {
                                    ok = true;
                                }
                            } else {
                                ok = true;
                            }
                        }
        //             }
                    if(ok == true){
                        idConsultation = idConsult;
                        mapConsult[idConsultation] = consult;
                        arrayConsult.push(idConsultation);
                    } else {
                        return();
                    } 
                }
            }
        //}
    }

    //MEDICO ACEITA UMA CONSULTA - TESTADO
    function doctorAccept(bytes memory consult) public{
        RLPReader.RLPItem[] memory ls = consult.toRlpItem().toList();
        uint idConsult = ls[0].toUint();
        arrayConsultAccept.push(idConsult);
        //Passa a consulta para a lista de consultas aceitas
        mapConsultAccept[idConsult] = consult;

        deleteArray(idConsult);
        delete mapConsult[idConsult];
    }

    //MEDICO REJEITA UMA CONSULTA - TESTADO
    function doctorRejects(bytes memory consult) public{
        RLPReader.RLPItem[] memory ls = consult.toRlpItem().toList();
        uint idConsult = ls[0].toUint();
        deleteArray(idConsult);
        delete mapConsult[idConsult];
    }

    //RETORNA TODAS AS CONSULTAS - TESTADO
    function getConsults() public view returns(bytes[] memory consult){
        uint i;
        bytes[] memory consults = new bytes[](arrayConsult.length);
        for(i = 0; i < arrayConsult.length ; i ++){
            consults[i] = mapConsult[arrayConsult[i]];
        }
        return(consults);
    }

    /////////////////////////////////////////////////////////// VERIFICAÇÔES ///////////////////////////////////////////////////////////
    function idConsultExists(uint idConsult) public view returns(bool){
        uint i;
        if(arrayConsult.length == 0 ){
            return false;
        } else {
            for(i = 0; i < arrayConsult.length; i ++){
                RLPReader.RLPItem[] memory ls = mapConsult[arrayConsult[i]].toRlpItem().toList();
                uint id_Consult = ls[0].toUint();
                if(idConsult == id_Consult){
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    //VERIFICA SE A DATA DA CONSULTA JÁ EXISTE - TESTADO
    function dateExists(uint dateConsult) public view returns(bool){
        uint i;
        for(i = 0; i < arrayConsult.length; i ++){
            RLPReader.RLPItem[] memory ls = mapConsult[arrayConsult[i]].toRlpItem().toList();
            uint date_Consult = ls[3].toUint();
            if(dateConsult == date_Consult){
                return true;
            } else {
                return false;
            }
        }
    }

    //VERIFICA SE A HORA DA CONSULTA JÁ EXISTE - TESTADO
    function hourExists(uint hour) public view returns(bool){
        uint i;
        for(i = 0; i < arrayConsult.length; i ++){
            RLPReader.RLPItem[] memory ls = mapConsult[arrayConsult[i]].toRlpItem().toList();
            uint hourConsult = ls[4].toUint();
            if(hour == hourConsult){
                return true;
            } else {
                return false;
            }
        }
    }

    //MEDICO RECEBE TODAS AS CONSULTAS PENDENTES - TESTADO
    function doctorReceives() public view returns(bytes[] memory consult){
        uint i;
        bytes[] memory consults = new bytes[](arrayConsult.length);
        for(i = 0; i < arrayConsult.length ; i ++){
            consults[i] = mapConsult[arrayConsult[i]];
        }
        return(consults);
    }

    //Deleta do array de consultas pendentes - TESTADO
    function deleteArray(uint idConsult) public{
        uint i;
        uint j = 0;
        uint id;
        uint[] memory arrayTemp = new uint[](arrayConsult.length - 1);
        for(i = 0; i < arrayConsult.length ; i ++){
            RLPReader.RLPItem[] memory ls = mapConsult[arrayConsult[i]].toRlpItem().toList();
            uint id_Consult = ls[0].toUint();
            if(idConsult == id_Consult){
                id = i;
            }else {
                arrayTemp[j] = id_Consult;
                j++;
            }
        }
        delete arrayConsult;
        arrayConsult = arrayTemp;
    }

    //PEGA UMA CONSULTA ESPECÌFICA - TESTADO
    function getConsult(uint idConsult) public view returns(bytes memory consult){
        uint i;
        for(i = 0; i < arrayConsult.length ; i ++){
            RLPReader.RLPItem[] memory ls = mapConsult[arrayConsult[i]].toRlpItem().toList();
            uint id_Consult = ls[0].toUint();
            if(idConsult == id_Consult){
                return mapConsult[arrayConsult[i]];
            }
        }
        return "erro";
    }


    //RETORNA O NÚMERO DE CONSULTAS - TESTADO
    function numberConsults() public view returns(uint consult){
        return (arrayConsult.length);
    }

    //PEGA UMA CONSULTA ACEITA ESPECÍFICA - TESTADO
    function getConsultAccept(uint idConsultAccept) public view returns(bytes memory consultAccept){
        uint i;

        for(i = 0; i < arrayConsultAccept.length ; i ++){
            RLPReader.RLPItem[] memory ls = mapConsultAccept[arrayConsultAccept[i]].toRlpItem().toList();
            uint id_Consult = ls[0].toUint();
            if(idConsultAccept == id_Consult){
                return mapConsultAccept[arrayConsultAccept[i]];
            }
        }
    }

    //PEGA TODAS AS CONSULTAS ACEITAS - TESTADO
    function getConsultsAccept() public view returns(bytes[] memory consult){
        uint i;
        bytes[] memory consults = new bytes[](arrayConsultAccept.length);
        for(i = 0; i < arrayConsultAccept.length ; i ++){
            consults[i] = mapConsultAccept[arrayConsultAccept[i]];
        }
        return(consults);
    }

/////////////////////////////////////////////////////////////////////código velho/////////////////////////////////////////////////////////////////////



    // function doctorReceives
    // (uint idConsult, uint idPat, uint idDoc, uint dateConsult, uint hourConsult, string memory patName, string memory docName)
    // public atStage(Stages.WaitingAccept) returns(uint, uint, uint, uint, string memory, string memory, bool accept){
    //     // Envio para o médico e retorno dele

    //     //para construir a mensagem enviada ao médico
    //     Consultation storage consult = mapConsult[idConsult];
    //     consult.idConsult = idConsult;
    //     consult.idPat = idPat;
    //     consult.idDoc = idDoc;
    //     consult.dateConsult = dateConsult;
    //     consult.hourConsult = hourConsult;
    //     consult.patName = patName;
    //     consult.docName = docName;
    //     consult.accept = false;
    //     arrayConsult.push(idConsult);
    //     //ler resposta
    //     return(idPat, idDoc, dateConsult, hourConsult, patName, docName, false);
    //     //se sim, prosseguir para a criação da consulta, pedindo para que o médico informe o id da consulta
    //     //se não, retornar ao paciente que seu pedido foi negado
    // }

    // function doctorAccept(uint idConsult) public atStage(Stages.Accept){
    //     uint i;
    //     for(i = 0; i < arrayConsult.length; i ++){
    //         if(mapConsult[i].idConsult == idConsult){
    //             new mapConsultAccept = mapConsult[i];
       
    // function nextStage() internal {
    //     stage = Stages(uint(stage) + 1);
    // } uint i;
    //     for(i = 0; i < arrayConsult.length; i ++){
    //         if(mapConsult[i].idConsult == idConsult){
    //             delete mapConsult[i];
    //         }
    //     }
    // }

    // function searchConsult(uint index) public view returns(uint id, string memory name, string memory healthPlan){

    // }

    // function nextStage() internal {
    //     stage = Stages(uint(stage) + 1);
    // }
}