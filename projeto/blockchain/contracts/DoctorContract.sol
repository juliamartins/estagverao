pragma solidity >=0.4.0 <0.7.0;
pragma experimental ABIEncoderV2;
import '../node_modules/solidity-rlp/contracts/RLPReader.sol';

contract DoctorContract {
    uint storedData;

    using RLPReader for RLPReader.RLPItem;
    using RLPReader for bytes;

    // struct Doctor { // Struct
    //         uint id;
    //         string name;
    //         string specialty;
    //         string healthPlan;
    //         //address addressMed;
    //     }

    // mapping (uint => Doctor) mapDoc;
    
    uint idDoctor;

    constructor() public{
        idDoctor = 0;
    }

    mapping (uint => bytes) mapDoc;
    uint[] arrayDoc;

    function returnIDDoc() public view returns(uint){
        return idDoctor;
    }

    function addDoctor(bytes memory doctor) public {
        uint i;
        bool double;
        double == false;

        RLPReader.RLPItem[] memory ls = doctor.toRlpItem().toList();
        uint id = ls[0].toUint();
        string memory name = string(ls[1].toBytes());
        string memory specialty = string(ls[2].toBytes());
        string memory healthPlan = string(ls[3].toBytes());

        string memory teste = "";

        if(id == 0) {
            return();
        } else {
            if(keccak256(abi.encodePacked(specialty)) == keccak256(abi.encodePacked(teste)) || keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked(teste)) || keccak256(abi.encodePacked(healthPlan)) == keccak256(abi.encodePacked(teste))){
                return();
            }else{
                for(i = 0; i < arrayDoc.length ; i ++){
                    if(arrayDoc[i]==id){
                        double = true;
                    }
                }
                if(double==true){
                    return();
                } else{
                    idDoctor = id;
                    mapDoc[id] = doctor;
                    arrayDoc.push(id);
                }  
            }
        } 
    }

    function doctorExists(uint id) public view returns(bool resp){
        uint i;
        for(i = 0; i< arrayDoc.length ; i ++){
            RLPReader.RLPItem[] memory ls = mapDoc[arrayDoc[i]].toRlpItem().toList();
            uint idDoc = ls[0].toUint();
            if(idDoc == id){
                return true;
            } else{
                return false;
            }
        }
    }

    function numberofDoctors() public view returns(uint n){
        return (arrayDoc.length);
    }

    function getDoctor(uint index) public view returns(bytes memory doctor){
        return (mapDoc[index]);
    }

    function getDoctors() public view returns(bytes[] memory doctors){
        uint i;
        bytes[] memory doctorsList = new bytes[](arrayDoc.length);
        
        for(i=0; i<arrayDoc.length ; i++){
            doctorsList[i] = mapDoc[arrayDoc[i]];
        }
        return doctorsList;
    }

    function getDoctorsbySpec(string memory spec) public view returns(bytes[] memory doctor){
        uint i = 0;
        uint j;
        uint cont;
        string memory speciality;
        
        for(i=0; i<arrayDoc.length; i++){
            RLPReader.RLPItem[] memory ls = mapDoc[arrayDoc[i]].toRlpItem().toList();
            speciality = string(ls[2].toBytes());
            if(keccak256(abi.encodePacked(speciality)) == keccak256(abi.encodePacked(spec))){
                cont++;
            }
        }

        bytes[] memory doctors = new bytes[](cont);

        for(i=0; i<arrayDoc.length; i++){
            RLPReader.RLPItem[] memory ls = mapDoc[arrayDoc[i]].toRlpItem().toList();
            speciality = string(ls[2].toBytes());
            if(keccak256(abi.encodePacked(speciality)) == keccak256(abi.encodePacked(spec))){
                doctors[j] = mapDoc[arrayDoc[i]];
                j++;
            }
        }
        return(doctors);
    }

    function getDoctorsbyName(string memory n) public view returns(bytes[] memory doctorsbyName){
        uint i = 0;
        uint j;
        uint cont;
        string memory name;
        
        for(i=0; i<arrayDoc.length; i++){
            RLPReader.RLPItem[] memory ls = mapDoc[arrayDoc[i]].toRlpItem().toList();
            name = string(ls[1].toBytes());
            if(keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked(n))){
                cont++;
            }
        }

        bytes[] memory doctors = new bytes[](cont);

        for(i=0; i<arrayDoc.length; i++){
            RLPReader.RLPItem[] memory ls = mapDoc[arrayDoc[i]].toRlpItem().toList();
            name = string(ls[1].toBytes());
            if(keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked(n))){
                doctors[j] = mapDoc[arrayDoc[i]];
                j++;
            }
        }
        return(doctors);
    }

    function getDoctorsbyHP(string memory hp) public view returns(bytes[] memory doctor){
        uint i = 0;
        uint j;
        uint cont;
        string memory healthPlan;
        
        for(i=0; i<arrayDoc.length; i++){
            RLPReader.RLPItem[] memory ls = mapDoc[arrayDoc[i]].toRlpItem().toList();
            healthPlan = string(ls[3].toBytes());
            if(keccak256(abi.encodePacked(healthPlan)) == keccak256(abi.encodePacked(hp))){
                cont++;
            }
        }

        bytes[] memory doctors = new bytes[](cont);

        for(i=0; i<arrayDoc.length; i++){
            RLPReader.RLPItem[] memory ls = mapDoc[arrayDoc[i]].toRlpItem().toList();
            healthPlan = string(ls[3].toBytes());
            if(keccak256(abi.encodePacked(healthPlan)) == keccak256(abi.encodePacked(hp))){
                doctors[j] = mapDoc[arrayDoc[i]];
                j++;
            }
        }
        return(doctors);
    }

    // function updateDoctor(uint id, string memory name, string memory specialty, string memory healthPlan) public{
    //     uint i;
    //     uint idDoc;
    //     for(i=0; i<arrayDoc.length ; i++){
    //         idDoc = mapDoc[arrayDoc[i]].id;
    //         if(keccak256(abi.encodePacked(idDoc)) == keccak256(abi.encodePacked(id))){
    //             mapDoc[arrayDoc[i]].name = name;
    //             mapDoc[arrayDoc[i]].specialty = specialty;
    //             mapDoc[arrayDoc[i]].id = id;
    //             mapDoc[arrayDoc[i]].healthPlan = healthPlan;
    //         }
    //     }
    // }

    // function getDoctorName(uint index) public view returns(string memory name){
    //     return (mapDoc[index].name);
    // }

    // function getDoctorHP(uint index) public view returns(string memory healthPlan){
    //     return (mapDoc[index].healthPlan);
    // }
}