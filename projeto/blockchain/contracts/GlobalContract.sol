pragma solidity >=0.4.0 <0.7.0;
pragma experimental ABIEncoderV2;
import './PatientContract.sol';
import './DoctorContract.sol';
import './ConsultationContract.sol';

contract GlobalContract {
    uint storedData;
    PatientContract patientContract;
    DoctorContract doctorContract;
    ConsultationContract consultationContract;

constructor(address PatientAddress, address DoctorAddress, address ConsultAddress) public{

    patientContract = PatientContract(PatientAddress);
    doctorContract = DoctorContract(DoctorAddress);
    consultationContract = ConsultationContract(ConsultAddress);
    }

//////////////////////////////////////////////////////////// Patients Functions ////////////////////////////////////////////////////////////
    function addPatient(bytes memory pacient) public{
        return patientContract.addPatient(pacient);
    }

    function returnID() public view returns(uint id){
        return patientContract.returnID();
    }

    function numberOfPatients() public view returns(uint n){
        return patientContract.numberPat();
    }

    function getPatient(uint index) public view returns(bytes memory patient){
        return patientContract.getPatient(index);
    }

    function getPatients() public view returns(bytes[] memory patients){
        return patientContract.getPatients();
    }

    function getPatientsbyName(string memory name) public view returns(bytes[] memory patients){
        return patientContract.getPatientsbyName(name);
    }

    function getPatientsbyHP(string memory hp) public view returns(bytes[] memory patients){
        return patientContract.getPatientsbyHP(hp);
    }

//     function uptadePatient(uint id, string memory name, string memory healthPlan) public{
//         return patientContract.updatePatient(id, name, healthPlan);
//     }

// //////////////////////////////////////////////////////////// Doctors Functions ////////////////////////////////////////////////////////////

    function numberOfDoctors() public view returns(uint n){
        return doctorContract.numberofDoctors();
    }

    function addDoctor(bytes memory doctor) public {
        return doctorContract.addDoctor(doctor);
    }

    function returnIDDoc() public view returns(uint id){
        return doctorContract.returnIDDoc();
    }

    function getDoctor(uint index) public view returns(bytes memory doctor){
        return doctorContract.getDoctor(index);
    }

    function getDoctors() public view returns(bytes[] memory doctor){
        return doctorContract.getDoctors();
    }

    function getDoctorsbySpec(string memory specialty) public view returns(bytes[] memory doctor){
        return doctorContract.getDoctorsbySpec(specialty);
    }

    function getDoctorsbyName(string memory name) public view returns(bytes[] memory doctor){
        return doctorContract.getDoctorsbyName(name);
    }

    function getDoctorsbyHP(string memory hp) public view returns(bytes[] memory doctor){
        return doctorContract.getDoctorsbyHP(hp);
    }

//     function updateDoctor(uint id, string memory name, string memory specialty, string memory healthPlan) public {
//         return doctorContract.updateDoctor(id, name, specialty, healthPlan);
//     }

//////////////////////////////////////////////////////////// Consults functions ////////////////////////////////////////////////////////////

    function createRequest(bytes memory consult) public {
        return consultationContract.createRequest(consult);
    }

    function returnIDConsult() public view returns(uint id){
        return consultationContract.returnIDConsult();
    }

    function doctorReceives() public returns(bytes[] memory arrayConsults){
        return consultationContract.doctorReceives();
    }

    function doctorAccept(bytes memory consult) public {
        return consultationContract.doctorAccept(consult);
    }

    function doctorRejects(bytes memory consult) public{
        return consultationContract.doctorRejects(consult);
    }

    function getConsult(uint idConsult) public view returns(bytes memory arrayConsults){
        return consultationContract.getConsult(idConsult);
    }

    function getConsultAccept(uint idConsultAccept) public view returns(bytes memory arrayConsults){
        return consultationContract.getConsultAccept(idConsultAccept);
    }

    function getConsultsAccept() public view returns(bytes[] memory arrayConsults){
        return consultationContract.getConsultsAccept();
    }

    function getConsults() public view returns(bytes[] memory arrayConsults){
        return consultationContract.getConsults();
    }

    function numberOfConsults() public view returns(uint n){
        return consultationContract.numberConsults();
    }
}