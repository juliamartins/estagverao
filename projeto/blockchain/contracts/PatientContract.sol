pragma solidity >=0.4.0 <0.7.0;
pragma experimental ABIEncoderV2;
import '../node_modules/solidity-rlp/contracts/RLPReader.sol';

contract PatientContract {
    uint storedData;

    using RLPReader for RLPReader.RLPItem;
    using RLPReader for bytes;

    // struct Patient { // Struct
    //     uint id;
    //     string name;
    //     string healthPlan;
    // }
    //mapping (uint => Patient) mapPat;
    mapping (uint => bytes) mapPat;

    uint[] arrayPat;

    uint idPatient;

    constructor() public{
        idPatient = 0;
    }

    function returnID() public view returns(uint){
        return idPatient;
    }

    function addPatient(bytes memory patient) public {
        uint i;
        bool double;
        double == false;

        RLPReader.RLPItem[] memory ls = patient.toRlpItem().toList();

        uint id = ls[0].toUint();
        string memory name = string(ls[1].toBytes());
        string memory healthPlan = string(ls[2].toBytes());

        string memory teste = "";

        if(keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked(teste)) || keccak256(abi.encodePacked(healthPlan)) == keccak256(abi.encodePacked(teste))){
            return();
        } else{
            if(id == 0) {
                return();
            } else {
                for(i = 0; i < arrayPat.length ; i ++){
                    if(arrayPat[i]==id){
                        double = true;
                    }
                }
                if(double==true){
                    return();
                } else{
                    idPatient = id;
                    mapPat[id] = patient;
                    arrayPat.push(id);
                }
            }
        }
    }

    function patientExists(uint id) public view returns(bool resp){
        uint i;
        for(i = 0; i< arrayPat.length ; i ++){
            RLPReader.RLPItem[] memory ls = mapPat[arrayPat[i]].toRlpItem().toList();
            uint idPat = ls[0].toUint();
            if(idPat == id){
                return true;
            } else{
                return false;
            }
        }
    }

    //número de pacientes já cadastrados
    function numberPat() public view returns(uint n){
        return (arrayPat.length);
    }

    function getPatient(uint index) public view returns(bytes memory patient){
        return (mapPat[index]);
    }

    function getPatients() public view returns(bytes[] memory patients){
        uint i;
        //definir o tamanho do array
        bytes[] memory patientsList = new bytes[](arrayPat.length);
        
        for(i=0; i<arrayPat.length ; i++){
            patientsList[i] = mapPat[arrayPat[i]];
        }
        return patientsList;
    }

    function getPatientsbyName(string memory n) public view returns(bytes[] memory patientsbyName){
        uint i = 0;
        uint j;
        uint cont;
        string memory name;
        
        for(i=0; i<arrayPat.length; i++){
            RLPReader.RLPItem[] memory ls = mapPat[arrayPat[i]].toRlpItem().toList();
            name = string(ls[1].toBytes());
            if(keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked(n))){
                cont++;
            }
        }

        bytes[] memory patients = new bytes[](cont);

        for(i=0; i<arrayPat.length; i++){
            RLPReader.RLPItem[] memory ls = mapPat[arrayPat[i]].toRlpItem().toList();
            name = string(ls[1].toBytes());
            if(keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked(n))){
                patients[j] = mapPat[arrayPat[i]];
                j++;
            }
        }
        return(patients);
    }

     function getPatientsbyHP(string memory hp) public view returns(bytes[] memory patientByHP){
        uint i = 0;
        uint j;
        uint cont;
        string memory healthPlan;
        
        for(i=0; i<arrayPat.length; i++){
            RLPReader.RLPItem[] memory ls = mapPat[arrayPat[i]].toRlpItem().toList();
            healthPlan = string(ls[2].toBytes());
            if(keccak256(abi.encodePacked(healthPlan)) == keccak256(abi.encodePacked(hp))){
                cont++;
            }
        }

        bytes[] memory patients = new bytes[](cont);

        for(i=0; i<arrayPat.length; i++){
            RLPReader.RLPItem[] memory ls = mapPat[arrayPat[i]].toRlpItem().toList();
            healthPlan = string(ls[2].toBytes());
            if(keccak256(abi.encodePacked(healthPlan)) == keccak256(abi.encodePacked(hp))){
                patients[j] = mapPat[arrayPat[i]];
                j++;
            }
        }
        return(patients);
     }

    // function getPatientName(uint index) public view returns(string memory name){
    //     return (mapPat[index].name);
    // }

    // function getPatientHP(uint index) public view returns(string memory healthPlan){
    //     return (mapPat[index].healthPlan);
    // }

}