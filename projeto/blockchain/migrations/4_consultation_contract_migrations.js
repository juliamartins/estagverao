const ConsultationContract = artifacts.require("ConsultationContract");
const PatientContract = artifacts.require("PatientContract");
const DoctorContract = artifacts.require("DoctorContract");
module.exports = function(deployer) {
  deployer.deploy(ConsultationContract, PatientContract.address, DoctorContract.address);
};