const GlobalContract = artifacts.require("GlobalContract");
const ConsultationContract = artifacts.require("ConsultationContract");
const PatientContract = artifacts.require("PatientContract");
const DoctorContract = artifacts.require("DoctorContract");
module.exports = function(deployer) {
  deployer.deploy(GlobalContract, PatientContract.address, DoctorContract.address, ConsultationContract.address);
};