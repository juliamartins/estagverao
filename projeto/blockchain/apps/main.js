const program = require('commander');
const Web3 = require('web3');
let web3 = new Web3('http://localhost:8455');

const abi = require("../build/contracts/GlobalContract.json")
const address = ("0x32B282E0C2B9eFCB332E464908a6F86D78803e28")
const addressAccount = '0x00210c77d83318b0b8592bf7e6a90069639b5dbe';
let myContract = new web3.eth.Contract(abi.abi, address);
const gasPrice = '800000'



//Interface command line

///////////////////////////////////// Funções do Paciente ///////////////////////////////////

//método de escrita: send
// métodos de escrita precisam de gas
async function addPatient(id, name, healthPlan){
    myContract.methods.addPatient(id,name,healthPlan)
        .send({from: addressAccount ,gas: gasPrice})
        .then(result => {
            console.log("[addPatient] Result: %s", JSON.stringify(result))
        })
        .catch(result => {
            console.log("[addPatient] Error: %s", result);
    });
}

// //método de leitura/consulta: call
// // métodos de consulta não precisam de gas
async function getPatient(id){
    myContract.methods.getPatient(id).call({
        from: address,
        }). then(function(receipt){
            console.log(JSON.stringify(receipt))
    })
    //console.log('Paciente buscado: ' + id)
}

async function getPatients(){
    myContract.methods.getPatients().call({
        from: address,
        }). then(function(receipt){
            console.log(JSON.stringify(receipt))
        })
}

async function getPatientsbyName(name){
    myContract.methods.getPatientsbyName(name).call({
        from: address,
        }). then(function(receipt){
            console.log(JSON.stringify(receipt))
        })
}

async function getPatientsbyHP(hp){
    myContract.methods.getPatientsbyHP().call({
        from: address,
        }). then(function(receipt){
            console.log(JSON.stringify(receipt))
        })
}

async function numberOfPatients(){
    myContract.methods.numberOfPatients().call({
        from: address,
        }). then(function(receipt){
            console.log(JSON.stringify(receipt))
    })
}
///////////////////////////////////// Funções do Médico ///////////////////////////////////
async function addDoctor(id, name, specialty, healthPlan, gasPrice){
    myContract.methods.addDoctor(id,name,specialty,healthPlan).send({
    from: address,
    gas: gasPrice}).then(function(receipt){
        if(receipt.status)
            console.log('Add new doctor named %s, with id %s', name, id)
        else
            console.log('erros')
    })
}

async function numberOfDoctors(){
    myContract.methods.numberOfDoctors().call({
        from: address,
        }). then(function(receipt){
            console.log(JSON.stringify(receipt))
        })
}

async function getDoctor(id){
    myContract.methods.getDoctor(id).call({
        from: address,
        }). then(function(receipt){
            console.log(JSON.stringify(receipt))
        })
}

async function getDoctors(){
    myContract.methods.getDoctors().call({
        from: address,
        }). then(function(receipt){
            console.log(JSON.stringify(receipt))
        })
}

async function main(){

    program 
        .version('0.0.1')
        .description('agendamento command line frontend')

    ///////////////////////////////////// Paciente ///////////////////////////////////
    program
        .command('addPatient <id> <name> <healthPlan>')
        .description('adicionar paciente')
        .action(addPatient)
        .action((patientAdd)=>{
            console.log('Operação realizada')
        });

    program
        .command('getPatient <id>')
        .description('buscar paciente por id')
        .action(getPatient)

    program
        .command('getPatients')
        .description('todos os pacientes')
        .action(getPatients)

    program
        .command('getPatientsbyName')
        .description('todos os pacientes')
        .action(getPatientsbyName)

    program
        .command('getPatientsbyHP')
        .description('todos os pacientes')
        .action(getPatientsbyHP)

    program
        .command('numberOfPatients')
        .description('número de pacientes')
        .action(numberOfPatients)

    /////////////////////////////////// Médico ///////////////////////////////////
    program
        .command('addDoctor <id> <name> <specialty>')
        .description('adicionar médico')
        .action(addDoctor)

    program
        .command('numberOfDoctors')
        .description('número de médicos')
        .action(numberOfDoctors)

    program
        .command('getDoctor <id>')
        .description('buscar médico por id')
        .action(getDoctor)

    program
        .command('getDoctors')
        .description('todos os médicos')
        .action(getDoctors)

    program.parse(process.argv);
}

process.on('unhandledRejection', console.log)

main();