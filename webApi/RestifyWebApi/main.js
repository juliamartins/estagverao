const restify = require('restify');
const config  = require('./config.js');
const webapi  = require('./src');
//const utils = require('./src/utils.js');

webapi.logging.logger.level = config.logLevel;

//utils.decryptPrivateKeys(config.ethereum.nodeEndPoints, config.ethereum.keystoreBasePath, config.ethereum.accountPassword);

const server = restify.createServer({
  log: webapi.logging.logger,
});

const socketio = require('socket.io')(server.server);

server.use(restify.plugins.bodyParser());
server.use(restify.plugins.dateParser());
server.use(restify.plugins.queryParser());
server.pre(restify.pre.sanitizePath());

//Patient
  server.post('/api/GlobalContract/addPatient', webapi.GlobalContract.addPatient);
  server.get('/api/GlobalContract/getPatient/:id', webapi.GlobalContract.getPatient);
  server.get('/api/GlobalContract/getPatients', webapi.GlobalContract.getPatients);
  server.get('/api/GlobalContract/getPatientsbyName/:name', webapi.GlobalContract.getPatientsbyName);
  server.get('/api/GlobalContract/getPatientsbyHP/:healthPlan', webapi.GlobalContract.getPatientsbyHP);

 //Doctor
  server.post('/api/GlobalContract/addDoctor', webapi.GlobalContract.addDoctor);
  server.get('/api/GlobalContract/getDoctor/:id', webapi.GlobalContract.getDoctor);
  server.get('/api/GlobalContract/getDoctors', webapi.GlobalContract.getDoctors);
  server.get('/api/GlobalContract/getDoctorsbyName/:name', webapi.GlobalContract.getDoctorsbyName);
  server.get('/api/GlobalContract/getDoctorsbySpec/:specialty', webapi.GlobalContract.getDoctorsbySpec);
  server.get('/api/GlobalContract/getDoctorsbyHP/:healthPlan', webapi.GlobalContract.getDoctorsbyHP);
//  server.get('/api/GlobalContract/numberOfDoctors', webapi.GlobalContract.numberOfDoctors);
//  server.get('/api/GlobalContract/updateDoctor/:id,name,spec,hp', webapi.GlobalContract.updateDoctor);

//Consult
  server.post('/api/GlobalContract/createRequest', webapi.GlobalContract.createRequest);
  server.post('/api/GlobalContract/doctorRejects', webapi.GlobalContract.doctorRejects);
  server.post('/api/GlobalContract/doctorAccept', webapi.GlobalContract.doctorAccept);
  server.get('/api/GlobalContract/getConsults', webapi.GlobalContract.getConsults);

// webapi.blockchainEvents.initialize(config.ethereum.eventNodeListenDefault,
//   ['AddPatient'],
//   socketio,
//   webapi.logging
//   );

socketio.sockets.on('connection', function (socket) {
  socket.emit('connected', { hi: 'user' });
});



server.listen(config.port, function() {
  webapi.logging.logger.info('%s %s listening at %s', config.env, server.name, server.url);
});
