'use strict'

const path = require("path");

const GAS = 6000000;
const GAS_PRICE = 0;
const CHAIN_ID = 8995;
const NETWORK_INDEX = 8995;
// const ACCOUNT_NODE_05 = "0x007dc0112a20bfcb38da1dc1f26d697ed1cf0761";
const GlobalContract_abi = require(path.resolve("../../projeto/blockchain/build/contracts/GlobalContract.json"));
// const keystoreBasePath = "../../blockchain/network-dapp/parity/nodes/data/";
const accountPassword = "123456";

const PATIENT_PROPERTIES = ["id", "name", "healthPlan"];
const DOCTOR_PROPERTIES = ["id", "name", "specialty", "healthPlan"];

const nodes_dev = [
    { 'name': 'Node Single', 'account': '0x00210c77d83318b0b8592bf7e6a90069639b5dbe', 'index': 0, 'endPoint': 'http://localhost:8455', 'endPointWs': 'ws://localhost:8555', 'keyFolder': '05/keys/poa_chain/' }
];

// const validatorNodes = [
//     { 'name': 'Node 0', 'account': '0x00a2a5c7ac0529360fe5301ac3b9af438e282b91', 'index': 0, 'endPoint': 'http://localhost:8450', 'endPointWs': 'ws://localhost:8550', 'keyFolder': '00/keys/poa_chain/' },
//     { 'name': 'Node 1', 'account': '0x00dd8121982b3d5a001c0d9558d4842c41155cd1', 'index': 1, 'endPoint': 'http://localhost:8451', 'endPointWs': 'ws://localhost:8551', 'keyFolder': '01/keys/poa_chain/' },
//     { 'name': 'Node 2', 'account': '0x00970f06752494c0a00d2e076703ab5d5e42ea1e', 'index': 2, 'endPoint': 'http://localhost:8452', 'endPointWs': 'ws://localhost:8552', 'keyFolder': '02/keys/poa_chain/' },
//     { 'name': 'Node 3', 'account': '0x008d45220c6cee05b14ba75010c6b1833acef2b3', 'index': 3, 'endPoint': 'http://localhost:8453', 'endPointWs': 'ws://localhost:8553', 'keyFolder': '03/keys/poa_chain/' }
// ];


module.exports = {
    name: 'webapi',
    version: '0.0.1',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 5000,
    logLevel: 'debug',
    ethereum: {
        gas: GAS,
        gas_price: GAS_PRICE,
        chain_id: CHAIN_ID,
        networkIndex: NETWORK_INDEX,
        GlobalContract: GlobalContract_abi.networks[NETWORK_INDEX].address,
        //account: ACCOUNT_NODE_05,
        GlobalContractAbi: GlobalContract_abi,
        nodeEndPoints: !process.env.NODE_ENV ? nodes_dev : validatorNodes,
        patientProperties: PATIENT_PROPERTIES,
        doctorProperties: DOCTOR_PROPERTIES,
        eventNodeListenDefault: 0,
        //keystoreBasePath: keystoreBasePath,
        accountPassword: accountPassword
    }
}
