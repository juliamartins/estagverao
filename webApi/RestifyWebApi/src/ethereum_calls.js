// const abi = require('ethereumjs-abi');
const Web3 = require('web3');
// const axios = require('axios');
const config = require('../config.js');

// async function getNonce(account, endPoint, logger) {
//     if (nonces[account]) {
//         nonces[account]++;
//     }else{
//         nonces[account] = 0;
//         logger.info("added new nonce %s for account: %s", 0, account);
//     }

//     let web3 = new Web3(endPoint);
//     return await web3.eth.getCount(account).then(nonce => {
//         logger.info("nonce from blockchain: %s", nonce);

//         //check blockchain for updated nonce
//         if(nonce > nonces[account]){
//             nonces[account] = nonce;
//             logger.info("local cache nonce update: %s", nonce);
//         }

//         return nonce;
//     })
// }
// async function Count(account, endPoint) {
//     if (nonces[account]) {
//         nonces[account]++;
//         return nonces[account];
//     }
//     let nonce = await axios({
//         method: "post",
//         url: endPoint,
//         headers: {
//             "Content-Type": "application/json"
//         },
//         data: {
//             jsonrpc: "2.0",
//             method: "eth_getCount",
//             params: [account, "latest"],
//             id: 1
//         }
//     });
//     return parseInt(nonce.data.result);
// }
// let nonces = {}
// let rrNode = 0;
module.exports = {
    selectNodeValidator: (node) => {
        if (!node) {
            node = 0;
        }
        // suffle a default node
        else if (node < 0 || node >= config.ethereum.nodeEndPoints.length) {
            rrNode = rrNode >= config.ethereum.nodeEndPoints.length ? 0 : rrNode;
            node = rrNode;
            rrNode++;
        }
        return config.ethereum.nodeEndPoints[node];
    }
    // , entity: (node) => {
    //     let contractAddress = config.ethereum.entityContract;
    //     let contractAbi = config.ethereum.entity_abi;
    //     let web3 = new Web3(node.endPoint);
    //     return new web3.eth.Contract(contractAbi.abi, contractAddress);
    // }
    // , async sendSignedEntity(from, key, contractAddress, method, types, params, node, resolveCallback, logger) {
    //     let web3 = new Web3(node.endPoint);
    //     let _data = "0x" + abi.methodID(method, types).toString('hex') + abi.rawEncode(types, params).toString('hex');
    //     let nonce = await getNonce(from, node.endPoint, logger);

    //     logger.info("account: %s  nonce: %s", from, nonce);

    //     let tx = {
    //         nonce: nonce,
    //         gasPrice: config.ethereum.gasPrice,
    //         gas: config.ethereum.gas,
    //         to: contractAddress,
    //         value: 0,
    //         data: _data,
    //         chainId: config.ethereum.chainId
    //     };

    //     return await web3.eth.accounts.signTransaction(tx, key).then(async result => {
    //         logger.info("sendSignedEntity: node %s account: %s, nonce: %s", node.endPoint, from.toString('hex'), nonce);
    //         return await web3.eth.sendSignedTransaction(result.rawTransaction, resolveCallback);
    //     });
    // }
    , myEvent: (node) => {
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPointWs);
        let provider = web3.currentProvider;
        provider.on("error", e => handleDisconnects(e));
        provider.on("close", e => handleDisconnects(e));
        provider.on('connect', function () { console.log('WS Connected'); });
        provider.on('disconnect', function () { console.log('WS Disconnected'); });
        return new web3.eth.Contract(contractAbi.abi, contractAddress);
    }
}
function handleDisconnects(e) { console.log("error", e); }