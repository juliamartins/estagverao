module.exports = {
    GlobalContract: require('./handlers/GlobalContract.js'),
    logging: require('./logger.js'),
    blockchainEvents: require('./listeners/listener.js')
};