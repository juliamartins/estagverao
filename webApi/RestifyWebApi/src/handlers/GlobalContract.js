'use strict'

// const json = require('deterministic-json');
const _ = require('lodash');
const rlp = require('rlp');
const Web3 = require('web3');

// const blockchainNode = require('../ethereum_calls.js');
// const utils = require('../utils.js');
const config = require('../../config.js');

// const gas = config.ethereum.gas;

let greetingWord = '';

module.exports = {

    //////////////////////////////////////////////////////PATIENT//////////////////////////////////////////////////////

    numberOfPatients: async(req, res, next) => {
        req.log.info('[numberOfPatients] called');

        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;

        await contract.methods.numberOfPatients()
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[numberOfPatients] Result: " + result)
                res.send(result);
                next();
            })
            .catch(patient => {
                req.log.error("[numberOfPatients] Error: ", result);
                next(result);
        });
    },



    // POST api/GlobalContract/addPatient
    addPatient: async (req, res, next) => {
        req.log.info('[addPatient] called')
        
        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let numberID;

        await contract.methods.returnID()
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                numberID = parseInt(result) + 1;
                next();
            })

        let patient = rlp.encode([
            numberID,
            req.body.name,
            req.body.healthPlan
        ]);

        await contract.methods.addPatient(patient)
            .send({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[addPatient] Result: " + JSON.stringify(result))
                res.send(result);
                next();
            })
            .catch(result => {
                req.log.error("[addPatient] Error: ", result);
                next(result);
            });
    },


    getPatient: async (req, res, next) => {
        req.log.info('[getPatient] called');

        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let patient = []

        await contract.methods.getPatient(req.params.id)
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[getPatient] Result: " + result)

                    decoded = rlp.decode(result);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            healthPlan: decoded[2].toString()
                        };
                    patient.push(obj);

                res.send(patient);
                next();
            })
            .catch(result => {
                req.log.error("[getPatient] Error: ", result);
                next(result);
        });
    },

    getPatients: async (req, res, next) => {
        req.log.info('[getPatients] called');

        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let patient = []

        await contract.methods.getPatients()
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[getPatients] Result: " + result)

                for(let i = 0; i < result.length; i++){
                    decoded = rlp.decode(result[i]);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            healthPlan: decoded[2].toString()
                        };
                    patient.push(obj);
                    };
                    
                res.send(patient);
                next();
            })
            .catch(patient => {
                req.log.error("[getPatients] Error: ", patient);
                next(patient);
        });
    },


    getPatientsbyName: async (req, res, next) => {
        req.log.info('[getPatientsbyName] called');


        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let patient = []

        await contract.methods.getPatientsbyName(req.params.name)
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(namePat => {
                req.log.info("[getPatientsbyName] Result: " + namePat)
                req.log.info("[getPatientsbyName] Name: " + req.params.name)

                for(let i = 0; i < namePat.length; i++){
                    decoded = rlp.decode(namePat[i]);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            healthPlan: decoded[2].toString()
                        };
                    patient.push(obj);
                    };
                    
                res.send(patient);
                next();
            })
            .catch(patient => {
                req.log.error("[getPatientbyName] Error: ", patient);
                next(patient);
        });
    },

    getPatientsbyHP: async (req, res, next) => {
        req.log.info('[getPatientsbyHP] called');


        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let patient = []

        await contract.methods.getPatientsbyHP(req.params.healthPlan)
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(spec => {
                req.log.info("[getPatientsbyHP] Result: " + spec)
                req.log.info("[getPatientsbyHP] Name: " + req.params.healthPlan)

                for(let i = 0; i < spec.length; i++){
                    decoded = rlp.decode(spec[i]);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            healthPlan: decoded[2].toString()
                        };
                    patient.push(obj);
                    };
                    
                res.send(patient);
                next();
            })
            .catch(patient => {
                req.log.error("[getPatientbyName] Error: ", patient);
                next(patient);
        });
    },

    // updatePatient: async (req, res, next) => {
    //     req.log.info('[updatePatient] called')
        
    //     let node = config.ethereum.nodeEndPoints[0];
    //     let contractAddress = config.ethereum.GlobalContract;
    //     let contractAbi = config.ethereum.GlobalContractAbi;
    //     let web3 = new Web3(node.endPoint);
    //     let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

    //     await contract.methods.updatePatient(req.body.id, req.body.name, req.body.healthPlan)
    //     .send({ from: node.account, gas: config.ethereum.gas })
    //         .then(result => {
    //             req.log.info("[updatePatient] Result: " + JSON.stringify(result))
    //             res.send(result);
    //             next();
    //         })
    //         .catch(result => {
    //             req.log.error("[updatePatient] Error: ", result);
    //             next(result);
    //     });
    // },

    ////////////////////////////////////////////////////// DOCTOR //////////////////////////////////////////////////////

    // POST api/GlobalContract/addDoctor
    addDoctor: async (req, res, next) => {
        req.log.info('[addDoctor] called')
            
        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let numberID;

        await contract.methods.returnIDDoc()
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                numberID = parseInt(result) + 1;
                next();
            })

        let doctor = rlp.encode([
            numberID,
            req.body.name,
            req.body.specialty,
            req.body.healthPlan
        ]);

        await contract.methods.addDoctor(doctor)
            .send({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[addDoctor] Result: " + JSON.stringify(result))
                res.send(result);
                next();
            })
            .catch(result => {
                req.log.error("[addDoctor] Error: ", result);
                next(result);
            });
    },

    getDoctor: async (req, res, next) => {
        req.log.info('[getDoctor] called');

        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let doctor = []

        await contract.methods.getDoctor(req.params.id)
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                
                req.log.info("[getDoctor] Result: " + result)
                    decoded = rlp.decode(result);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            specialty:  decoded[2].toString(),
                            healthPlan: decoded[3].toString()
                        };
                    doctor.push(obj);

                res.send(doctor);
                next();
            })
            .catch(result => {
                req.log.error("[getDoctor] Error: ", result);
                next(result);
        });
    },

    getDoctors: async (req, res, next) => {
        req.log.info('[getDoctors] called');

        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let doctor = []

        await contract.methods.getDoctors()
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[getDoctors] Result: " + result)

                for(let i = 0; i < result.length; i++){
                    decoded = rlp.decode(result[i]);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            specialty:  decoded[2].toString(),
                            healthPlan: decoded[3].toString()
                        };
                        doctor.push(obj);
                    };
                    
                res.send(doctor);
                next();
            })
            .catch(doctor => {
                req.log.error("[getDoctors] Error: ", doctor);
                next(doctor);
        });
    },

    getDoctorsbyName: async (req, res, next) => {
        req.log.info('[getDoctorsbyName] called');


        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let doctor = []

        await contract.methods.getDoctorsbyName(req.params.name)
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(nameDoc => {
                for(let i = 0; i < nameDoc.length; i++){
                    decoded = rlp.decode(nameDoc[i]);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            specialty:   decoded[2].toString(),
                            healthPlan: decoded[3].toString()
                        };
                        doctor.push(obj);
                    };
                    
                res.send(doctor);
                next();
            })
            .catch(doctor => {
                req.log.error("[getDoctorsbyName] Error: ", doctor);
                next(doctor);
        });
    },

    getDoctorsbySpec: async (req, res, next) => {
        req.log.info('[getDoctorsbySpec] called');


        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let doctor = []

        await contract.methods.getDoctorsbySpec(req.params.specialty)
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(nameDoc => {
                for(let i = 0; i < nameDoc.length; i++){
                    decoded = rlp.decode(nameDoc[i]);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            specialty:   decoded[2].toString(),
                            healthPlan: decoded[3].toString()
                        };
                        doctor.push(obj);
                    };
                    
                res.send(doctor);
                next();
            })
            .catch(doctor => {
                req.log.error("[getDoctorsbySpec] Error: ", doctor);
                next(doctor);
        });
    },

    getDoctorsbyHP: async (req, res, next) => {
        req.log.info('[getDoctorsbyHP] called');


        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let doctor = []

        await contract.methods.getDoctorsbyHP(req.params.healthPlan)
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(nameDoc => {
                for(let i = 0; i < nameDoc.length; i++){
                    decoded = rlp.decode(nameDoc[i]);
                        const obj = {
                            id:         decoded[0].readUIntBE(0, decoded[0].length),
                            name:       decoded[1].toString(),
                            specialty:  decoded[2].toString(),
                            healthPlan: decoded[3].toString()
                        };
                        doctor.push(obj);
                    };
                    
                res.send(doctor);
                next();
            })
            .catch(doctor => {
                req.log.error("[getDoctorsbyHP] Error: ", doctor);
                next(doctor);
        });
    },

    ////////////////////////////////////////////////////// CONSULTATION //////////////////////////////////////////////////////

    createRequest: async (req, res, next) => {
        req.log.info('[createRequest] called')
        
        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let numberID;

        await contract.methods.returnIDConsult()
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                numberID = parseInt(result) + 1;
                next();
            })

        let consult = rlp.encode([
            numberID,
            req.body.idPat,
            req.body.idDoc,
            req.body.dateConsult,
            req.body.hourConsult,
            req.body.namePat,
            req.body.nameDoc
        ]);

        await contract.methods.createRequest(consult)
            .send({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[createRequest] Result: " + JSON.stringify(result))
                res.send(result);
                next();
            })
            .catch(result => {
                req.log.error("[createRequest] Error: ", result);
                next(result);
            });
    },

      // POST api/GlobalContract/addPatient
    doctorRejects: async (req, res, next) => {
        req.log.info('[doctorRejects] called')
        
        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        // let numberID;

        // await contract.methods.returnIDConsult()
        // .call({ from: node.account, gas: config.ethereum.gas })
        //     .then(result => {
        //         numberID = parseInt(result) + 1;
        //         next();
        //     })

        let consult = rlp.encode([
            req.body.idConsult,
            req.body.idPat,
            req.body.idDoc,
            req.body.dateConsult,
            req.body.hourConsult,
            req.body.namePat,
            req.body.nameDoc
        ]);

        await contract.methods.doctorRejects(consult)
            .send({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[doctorRejects] Result: " + JSON.stringify(result))
                res.send(result);
                next();
            })
        .catch(result => {
            req.log.error("[doctorRejects] Error: ", result);
            next(result);
        });
},

    // POST api/GlobalContract/addPatient
    doctorAccept: async (req, res, next) => {
        req.log.info('[doctorAccept] called')
        
        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        // let numberID;

        // await contract.methods.returnIDConsult()
        // .call({ from: node.account, gas: config.ethereum.gas })
        //     .then(result => {
        //         numberID = parseInt(result) + 1;
        //         next();
        //     })

        let consult = rlp.encode([
            req.body.idConsult,
            req.body.idPat,
            req.body.idDoc,
            req.body.dateConsult,
            req.body.hourConsult,
            req.body.namePat,
            req.body.nameDoc
        ]);

        await contract.methods.doctorAccept(consult)
            .send({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[doctorAccept] Result: " + JSON.stringify(result))
                res.send(result);
                next();
            })
            .catch(result => {
                req.log.error("[doctorAccept] Error: ", result);
                next(result);
        });
    },

    getConsults: async (req, res, next) => {
        req.log.info('[getConsults] called');

        let node = config.ethereum.nodeEndPoints[0];
        let contractAddress = config.ethereum.GlobalContract;
        let contractAbi = config.ethereum.GlobalContractAbi;
        let web3 = new Web3(node.endPoint);
        let contract = new web3.eth.Contract(contractAbi.abi, contractAddress);

        let decoded;
        let consult = []

        await contract.methods.getConsults()
        .call({ from: node.account, gas: config.ethereum.gas })
            .then(result => {
                req.log.info("[getConsults] Result: " + result)

                for(let i = 0; i < result.length; i++){
                    decoded = rlp.decode(result[i]);
                        const obj = {
                            idConsult:      decoded[0].readUIntBE(0, decoded[0].length),
                            idPat:          decoded[1].readUIntBE(0, decoded[0].length),
                            idDoc:          decoded[2].readUIntBE(0, decoded[0].length),
                            dateConsult:    decoded[3].readUIntBE(0, decoded[0].length),
                            hourConsult:    decoded[4].readUIntBE(0, decoded[0].length),
                            namePat:        decoded[5].toString(),
                            nameDoc:        decoded[6].toString()
                        };
                        consult.push(obj);
                    };
                    
                res.send(consult);
                next();
            })
            .catch(consult => {
                req.log.error("[getConsults] Error: ", consult);
                next(consult);
        });
    },

}