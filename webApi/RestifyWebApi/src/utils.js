const _ = require('lodash');

module.exports = {
    arrayToObjects: (properties, values) => {
        return _.zipObject(properties, values);
    },
    arrayItemToString: (item) => {
        return item.toString();
    },
    arrayItemToUint: (item) => {
        return item.readUIntBE(0, 1);
    },
};



// const _ = require('lodash');
// // const keythereum = require('keythereum');
// // const path = require('path');

// module.exports = {
//     objectsToArray: (objects) => {
//         return _.chain(objects)
//         .map(object => {
//             const keys = Object.keys(object)
//             const sortedKeys = _.sortBy(keys)

//             return _.values(
//                     _.fromPairs(
//                         _.map(sortedKeys, key => [key, object[key]])
//                 )
//             )
//         })
//         .value();
//     },
//     arrayToObjects: (properties, values) => {
//         return _.zipObject(properties, values);
//     },
//     arrayItemToString: (item) => {
//         return item.toString();
//     },
//     arrayItemToUInt: (item) => {
//         return item.length > 0 ? item.readUIntBE(0, item.length): 0;
//     },
//     arrayItemToUint256: (item) => {
//         return item.readUIntBE(28, 4);
//     },
//     // decryptPrivateKeys: (nodeEndPoints, keystoreBasePath, accountPassword) => {
//     //     nodeEndPoints.map(node => {
//     //         let keystoreFullPath = path.resolve(keystoreBasePath + node.keyFolder);
//     //         let keyObject = keythereum.importFromFile(node.account, keystoreFullPath);
//     //         let privateKey = keythereum.recover(accountPassword, keyObject);
//     //         node.privKey = "0x" + privateKey.toString('hex');
//     //     });
//     // }
// };